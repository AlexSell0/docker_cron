#!/bin/sh

time=`date +'%Y-%m-%d'`
dirBackup=/var/backups

/bin/tar -cvzf $dirBackup/backup-file-$time.tar.gz /var/www/html

docker exec db /usr/bin/mysqldump -u root --password=root --all-databases > $dirBackup/backup-db-$time.sql